#!/usr/bin/env bash

set -ue

until PGPASSWORD=$POSTGRES_PASSWORD psql -h $POSTGRESS_HOST_SERVER -U $POSTGRES_USER -d 'horizon' -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

function horizon_init_db() {
  echo "Initializing Horizon database..."
  horizon db init || echo "Horizon database initialization failed (possibly because it has been done before)"
}


horizon_init_db

(sleep 5 ; supervisorctl restart horizon) & supervisord -n -c /horizon/supervisord.horizon.conf
